package ayataka;

import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIBase;

public class EntityAIMoveTowardsAyataka extends EntityAIBase {
    private EntityCreature theEntity;
    private double movePosX;
    private double movePosY;
    private double movePosZ;
    private double movementSpeed;
    public EntityAyatakaBase target;

    public EntityAIMoveTowardsAyataka(EntityCreature par1EntityCreature, double movementSpeed)
    {
        this.theEntity = par1EntityCreature;
        this.movementSpeed = movementSpeed;
        this.setMutexBits(1);
    }

	@Override
	public boolean shouldExecute() {
        if (this.theEntity.func_110173_bK() && !isEnable())
        {
            return false;
        }
        else
        {
            return this.theEntity.getNavigator().tryMoveToXYZ(target.posX, target.posY, target.posZ, this.movementSpeed);
        }
	}
	
	public boolean isEnable() {
		return (target != null && target.isEntityAlive());
	}
}
