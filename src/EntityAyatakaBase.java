package ayataka;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAITaskEntry;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityIronGolem;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntitySnowman;
import net.minecraft.entity.monster.EntityWitch;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityAyatakaBase extends EntityThrowable {
	private List<EntityAIMoveTowardsAyataka> ais = new ArrayList<EntityAIMoveTowardsAyataka>();
	
	private int injectTiming = 0;
	private int countDownToDeath = 200;
	
    public EntityAyatakaBase(World par1World)
    {
        super(par1World);
    }

    public EntityAyatakaBase(World par1World, EntityLivingBase par2EntityLivingBase)
    {
        super(par1World, par2EntityLivingBase);
		setAI();
    }

    public EntityAyatakaBase(World par1World, double par2, double par4, double par6)
    {
        super(par1World, par2, par4, par6);
    }

	@Override
	protected void onImpact(MovingObjectPosition movingobjectposition) {
        for (int j = 0; j < 8; ++j)
        {
            this.worldObj.spawnParticle("snowballpoof", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
        }
        
        inGround = true;
	}
	
	@Override
	public void onUpdate() {
		super.onUpdate();
		
        if (!this.worldObj.isRemote)
        {
        	if (injectTiming == 0) {
        		setAI();
        		injectTiming = 10;
        	}
        	injectTiming--;
        }
        
    	if (countDownToDeath == 0) {
    		//System.out.println("Death!!");
            for (int j = 0; j < 8; ++j)
            {
            	this.worldObj.spawnParticle("snowballpoof", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
            }
            this.worldObj.playSoundAtEntity(this, "random.wood_click", 0.5F, 0.5F);
            
    		setDead();
    	}
    	countDownToDeath--;
	}

	private void setAI() {
		List<EntityAIMoveTowardsAyataka> injected = injectAI(worldObj);
		for (EntityAIMoveTowardsAyataka ai : injected) {
			ai.target = this;
		}
	}
	
	@Override
	public void setDead() {
		for (EntityAIMoveTowardsAyataka ai : this.ais) {
			if(ai.target == this) {
				ai.target = null;
			}
		}
		super.setDead();
	}
	
    private List<EntityCreature> nearMobs(World world) {
    	AxisAlignedBB range = AxisAlignedBB.getAABBPool().getAABB(posX-16, posY-4, posZ-16, posX+16, posY+4, posZ+16);
    	List list = world.getEntitiesWithinAABB(EntityCreature.class, range);
    	return list;
    }

    private List<EntityAIMoveTowardsAyataka> injectAI(World world) {
    	List<EntityCreature> list = nearMobs(world);
    	List<EntityAIMoveTowardsAyataka> resAi = new ArrayList();
    	for (EntityCreature e : list) {
    		if (e instanceof EntityWitch || e instanceof EntityWither ||
    			e instanceof EntityIronGolem || e instanceof EntitySkeleton ||
    			e instanceof EntitySnowman || e instanceof EntityZombie ||
    			e instanceof EntityCreeper) {
    			EntityMob mob = (EntityMob) e;
    			EntityAIMoveTowardsAyataka ai = containedTask(mob);
    			if (ai == null) {
    				//System.out.println("Ayataka: injected MOB = " + mob.toString());
    				ai = new EntityAIMoveTowardsAyataka(mob, 1.0D);
    				mob.tasks.addTask(0, ai);
    				this.ais.add(ai);
    			}

    			if (!ai.isEnable()) {
    				resAi.add(ai);
    			}
    		}
    		else {
    			//System.out.println("Ayataka: moving MOB = " + e.toString());
    			e.setTarget(this);
    		}
		}
    	return resAi;
    }

	private EntityAIMoveTowardsAyataka containedTask(EntityMob mob) {
		List<EntityAITaskEntry> entries = mob.tasks.taskEntries;
		for (EntityAITaskEntry entry : entries) {
			if (entry.action instanceof EntityAIMoveTowardsAyataka) {
				return (EntityAIMoveTowardsAyataka) entry.action;
			}
		}
		return null;
	}
}
