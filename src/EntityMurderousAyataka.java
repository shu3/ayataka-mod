package ayataka;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;

public class EntityMurderousAyataka extends EntityAyatakaBase {

    public EntityMurderousAyataka(World par1World)
    {
        super(par1World);
    }

    public EntityMurderousAyataka(World par1World, EntityLivingBase par2EntityLivingBase)
    {
        super(par1World, par2EntityLivingBase);
    }

    public EntityMurderousAyataka(World par1World, double par2, double par4, double par6)
    {
        super(par1World, par2, par4, par6);
    }

    @Override
    public void onUpdate() {
    	if (this.rand.nextInt(2) == 0) {
    		worldObj.spawnParticle("smoke", posX, posY + 0.5F, posZ, 0.0D, 0.0D, 0.0D);
    	}
    	super.onUpdate();
    }
    
    @Override
    public void setDead() {
    	worldObj.createExplosion(this, posX, posY, posZ, 3.0F, true);
    	super.setDead();
    }
}
