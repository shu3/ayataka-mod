package ayataka;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemMurderousAyataka extends ItemAyataka {

	public ItemMurderousAyataka(int par1) {
		super(par1);
        setUnlocalizedName("MurderousAyataka");
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IconRegister par1IconRegister)
	{
		this.itemIcon = par1IconRegister.registerIcon(ModAyataka.ModId + ":murderous_ayataka");
	}

	@Override
	public void throwAyataka(World par2World, EntityPlayer par3EntityPlayer, float f) {
		EntityMurderousAyataka ayataka = new EntityMurderousAyataka(par2World, par3EntityPlayer);
		ayataka.setThrowableHeading(ayataka.motionX, ayataka.motionY, ayataka.motionZ, f*2.0F, 1.0F);
		par2World.spawnEntityInWorld(ayataka);
	}

}
