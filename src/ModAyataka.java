package ayataka;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityList;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid="Ayataka", name="Ayataka", version="0.0.1")
@NetworkMod(clientSideRequired=true, serverSideRequired=false, versionBounds="1.6.2")
public class ModAyataka {
    @Instance("Ayataka")
    public static ModAyataka instance;

    @SidedProxy(clientSide="ayataka.client.ClientProxy", serverSide="ayataka.CommonProxy")
    public static CommonProxy proxy;
    
    public static final String ModId = "Ayataka";
    
    public static int ayatakaID;
    public static int ayatakaMurderousID;
    
    public static Item ayatakaItem;
    public static Item ayatakaMurderousItem;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        try {
            Configuration config = new Configuration(event.getSuggestedConfigurationFile());
            config.load();
            this.ayatakaID = config.getItem("Ayataka", 740).getInt();
            this.ayatakaMurderousID = config.getItem("MurderousAyataka", 741).getInt();

            config.save();
        } catch (ExceptionInInitializerError e) {
        	System.out.println(e.getCause().getMessage());
        	e.getCause().printStackTrace();
        	throw e;
        }

    }

    @EventHandler
    public void load(FMLInitializationEvent event) {
        this.ayatakaItem = new ItemAyataka(this.ayatakaID);
        this.ayatakaMurderousItem = new ItemMurderousAyataka(this.ayatakaMurderousID);

        LanguageRegistry.addName(this.ayatakaItem, "Ayataka");
        LanguageRegistry.instance().addNameForObject(this.ayatakaItem, "ja_JP", "綾鷹");
        LanguageRegistry.addName(this.ayatakaMurderousItem, "MurderousAyataka");
        LanguageRegistry.instance().addNameForObject(this.ayatakaMurderousItem, "ja_JP", "殺意の波動に目覚めた綾鷹");

        GameRegistry.addRecipe(new ItemStack(this.ayatakaItem, 1),
                " K ", "KMK", " K ",
                'K', Item.wheat,
                'M', Item.bucketWater);

        GameRegistry.addRecipe(new ItemStack(this.ayatakaMurderousItem, 1),
                "GGG", "GAG", "GGG",
                'G', Item.gunpowder,
                'A', this.ayatakaItem);
        
        int entityId = 600;
        String entityName = "Ayataka";
		EntityRegistry.registerModEntity(EntityAyataka.class, entityName, entityId, this, 80, 3, true);
		LanguageRegistry.instance().addStringLocalization("entity.Ayataka.name", entityName);
		
        entityId = 601;
        entityName = "MurderousAyataka";
		EntityRegistry.registerModEntity(EntityMurderousAyataka.class, entityName, entityId, this, 80, 3, true);
		LanguageRegistry.instance().addStringLocalization("entity.MurderousAyataka.name", entityName);
		
        proxy.registerRenderers();
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
            // Stub Method
    }
}
