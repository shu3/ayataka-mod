package ayataka.client;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import net.minecraft.client.renderer.entity.RenderSnowball;
import net.minecraft.entity.projectile.EntitySnowball;
import net.minecraft.item.Item;
import ayataka.CommonProxy;
import ayataka.EntityAyataka;
import ayataka.EntityMurderousAyataka;
import ayataka.ModAyataka;

public class ClientProxy extends CommonProxy {
	// Client stuff
	public void registerRenderers() {
		System.out.println("Ayataka: registerRenderers");
		RenderingRegistry.registerEntityRenderingHandler(EntityMurderousAyataka.class, new RenderSnowball(ModAyataka.ayatakaMurderousItem));
		RenderingRegistry.registerEntityRenderingHandler(EntityAyataka.class, new RenderSnowball(ModAyataka.ayatakaItem));
	}
}
